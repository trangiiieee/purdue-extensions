## Requirements

- Node 8.11.1 (use [nvm](https://github.com/creationix/nvm) to manage your Node versions!)
- [Yarn](https://yarnpkg.com) `npm i -g yarn`

## Setting up

1. Clone/download to your machine
2. Run `yarn` to install dependencies

## Tasks

| Task                        | Description                                                                                                  |
|:----------------------------|:-------------------------------------------------------------------------------------------------------------|
| `yarn start` or `yarn dev`  | Watch for changes to new files.                                                                              |
| `yarn test`                 | Lints your CSS and JS files.                                                                                 |

## 🚨 IMPORTANT! Using Yarn vs. NPM

**Please note:** We are using Yarn on this project. This means **you should not use npm commands when adding/removing dependencies**! Please refer to the [Yarn's documentation](https://yarnpkg.com/en/docs/migrating-from-npm#toc-cli-commands-comparison) to ensure you are running the correct `yarn` command to:

- Add new dependencies
- Remove dependencies
- Reinstall dependencies